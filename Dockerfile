# copy of https://github.com/ajeetraina/prometheus-armv7 

FROM resin/rpi-raspbian:stretch
MAINTAINER "Ajeet Singh Raina" <ajeetraina@gmail.com>

RUN apt-get update && apt-get install -qy curl ca-certificates
WORKDIR /root/
RUN mkdir /root/prometheus

RUN curl -sSLO  https://github.com/prometheus/prometheus/releases/download/v2.4.3/prometheus-2.4.3.linux-armv7.tar.gz && \
   tar -xvf prometheus-*.tar.gz -C /root/prometheus/ --strip-components=1 && \
   rm prometheus-*.tar.gz

workdir /root/prometheus

RUN mkdir -p /usr/share/prometheus
RUN mkdir -p /etc/prometheus
RUN mv ./prometheus /usr/bin/
RUN mv ./promtool /usr/bin/
RUN mv ./console_libraries /usr/share/prometheus/
RUN mv ./consoles /usr/share/prometheus/

EXPOSE 9090 
VOLUME [ "/prometheus" ]
WORKDIR /prometheus
ENTRYPOINT [ "/usr/bin/prometheus" ]
CMD ["--config.file=/etc/prometheus/prometheus.yml", \
     "--storage.tsdb.path=/prometheus", \
     "--web.console.libraries=/usr/share/prometheus/console_libraries", \
     "--web.console.templates=/usr/share/prometheus/consoles" ] 
